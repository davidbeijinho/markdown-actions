---
title: watch/youtube/tech
alias: tech/youtube
playlistId: "PLRaaBir-Es2j8av8iDAE-2uQfggE4-klq"
---

- Oxidise Your Life
  id:: 64e45905-87d2-44d2-8651-601199d50096
  - https://youtu.be/dFkGNe4oaKk
  error sending video
- NixOS is Mindblowing
  id:: 64e45905-c593-4ed9-88ac-25eb60e26089
  - https://www.youtube.com/watch?v=fuWPuJZ9NcU
  error sending video
- Ask Me Anything devops
  id:: 64e45905-f2ed-4787-ba87-87f401e6b249
  - https://youtu.be/MxqT00Kt4\_Q
  error sending video
- TypeScript - The Basics
  id:: 64e45905-f8c4-4bea-b193-c3ac3ff127a1
  - https://youtu.be/ahCwqrYpIuM
  error sending video
- Kubernetes - Ask Me Anything
  id:: 64e45905-979b-4150-83f9-1a508b1a3fc8
  - https://youtu.be/2fMzQ8ve03I
  error sending video
