import { playlistItemsInsert } from "./google/youtube.js";
export const saveToPlaylist = async ({ metadata, text, }) => {
    if (!metadata.playlistId) {
        console.error("no playlist id found");
        process.exit(1);
    }
    try {
        const url = new URL(text);
        const pathHostnames = ["youtu.be", "www.youtu.be"];
        const paramsHostnames = ["youtube.com", "www.youtube.com"];
        const validHostnames = [...paramsHostnames, ...pathHostnames];
        if (!validHostnames.includes(url.hostname)) {
            return "hostname not valid";
        }
        let videoId;
        if (pathHostnames.includes(url.hostname)) {
            videoId = getIdFromPath(url);
        }
        else {
            videoId = getIdFromParams(url);
        }
        if (videoId?.length != 11) {
            return "video id not valid";
        }
        else {
            const response = await playlistItemsInsert({
                playlistId: metadata.playlistId,
                videoId,
            });
            console.log({ response });
            if (response === true) {
                return true;
            }
            return "error sending video";
        }
    }
    catch (error) {
        console.error("url is not valid", error);
        return "url is not valid";
    }
};
const getIdFromPath = (url) => {
    // console.log(url.pathname)
    return url.pathname.slice(1);
};
const getIdFromParams = (url) => {
    return url.searchParams.get("v");
};
//# sourceMappingURL=youtube-playlist.js.map