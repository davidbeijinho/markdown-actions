import { readFile, writeFile } from "./file.js";
import { fromMarkdown } from "mdast-util-from-markdown";
import { frontmatterFromMarkdown, frontmatterToMarkdown, } from "mdast-util-frontmatter";
import { frontmatter } from "micromark-extension-frontmatter";
import { toMarkdown } from "mdast-util-to-markdown";
let worker;
let metadata;
import { parse } from "yaml";
const parseDoc = (content) => {
    return fromMarkdown(content, {
        extensions: [frontmatter(["yaml"])],
        mdastExtensions: [frontmatterFromMarkdown(["yaml"])],
    });
};
const parseTree = (data) => {
    if (data.children.length !== 2) {
        console.error("document has not exactly 2 root children");
        process.exit(1);
    }
    const [yaml, list] = data.children;
    if (yaml.type != "yaml") {
        console.error("first element is not frontmatter");
        process.exit(1);
    }
    if (list.type != "list") {
        console.error("second element is not list");
        process.exit(1);
    }
    return { yaml, list };
};
const parseList = async (list) => {
    // console.log({children: list.children})
    return await {
        ...list,
        children: await Promise.resolve(list.children.reduce(parseItem, Promise.resolve([]))),
    };
};
const parseItem = async (previousItems, item) => {
    const items = await previousItems;
    //console.log({items, item})
    if (item.children.length != 2) {
        console.error("has not exactly 2 children");
        return true;
    }
    const [paragraph, list] = item.children;
    if (paragraph.type != "paragraph") {
        console.error("first element is not paragraph");
        return true;
    }
    if (list.type != "list") {
        console.error("second element is not list");
        return true;
    }
    const response = await parseLink(list);
    if (response) {
        if (response === true) {
            return items;
        }
        console.log({ items });
        return [
            ...items,
            {
                ...item,
                children: [paragraph, response],
            },
        ];
    }
    return [...items, item];
};
const parseLink = async (item) => {
    //console.log(item)
    if (item.children.length != 1) {
        console.error("has not exactly 1 children");
        return addError(item, "has not exactly 1 children");
    }
    const [listItem] = item.children;
    if (listItem.children.length != 1) {
        console.error("has not exactly 1 children");
        return addError(item, "has not exactly 1 children");
    }
    const [paragraph] = listItem.children;
    if (paragraph.type != "paragraph") {
        console.error("first element is not paragraph");
        return addError(item, "first element is not paragraph");
    }
    if (paragraph.children.length != 1) {
        console.error("has not exactly 1 children");
        return addError(item, "has not exactly 1 children");
    }
    const [text] = paragraph.children;
    if (text.type != "text") {
        console.error("first element is not text");
        return addError(item, "first element is not text");
    }
    const result = await worker({ metadata, text: text.value });
    if (result === false) {
        return addError(item, "not able to save");
    }
    if (typeof result === "string") {
        return addError(item, result);
    }
    return result;
};
const addError = (item, error) => {
    return {
        ...item,
        children: [...(item.children || []), { type: "text", value: error }],
    };
};
const saveFile = ({ mdast, yaml, updatedList, path }) => {
    const update = {
        ...mdast,
        children: [yaml, updatedList],
    };
    const out = toMarkdown(update, {
        bullet: "-",
        extensions: [frontmatterToMarkdown(["yaml"])],
    });
    writeFile({
        //path: `${path}-saved.md`,
        path,
        data: out,
    });
    console.log(out);
};
export const processFile = async (path, sender) => {
    const content = await readFile({ path });
    const mdast = parseDoc(content);
    // console.log({ mdast });
    const { yaml, list } = parseTree(mdast);
    metadata = parse(yaml.value);
    worker = sender;
    const updatedList = await parseList(list);
    saveFile({ mdast, yaml, updatedList, path });
    return updatedList;
};
//# sourceMappingURL=markdown.js.map