import { createServer } from "http";
import { generateAuthUrl, handleAuthCode, processTokensFile, } from "./google/google.js";
const handleLogin = (req, res) => {
    const url = generateAuthUrl(`http://${req.headers.host}/code`);
    // generateAuthUrl("")
    // console.log(req)
    // // console.log(req.host)
    // console.log(req.socket.remoteAddress)
    // console.log(req.socket.remotePort)
    // console.log(req.socket.address())
    // console.log(req.headers.host); // localhost:8080
    // console.log(req.headers.host.split(':')[1]); // 8080
    res.writeHead(200);
    res.end(JSON.stringify({ url }));
};
const handleAuth = async (req, res) => {
    if (!req.url) {
        console.log("no url returned", req);
        return;
    }
    const query = req.url.slice("/code".length + 1);
    console.log({ query });
    const searchParams = new URLSearchParams(query);
    const code = searchParams.get("code");
    console.log({ code });
    if (code) {
        try {
            const response = await handleAuthCode(code);
            console.log(response);
            res.writeHead(200);
            res.end(JSON.stringify({ msg: "done" }));
        }
        catch (error) {
            res.writeHead(500);
            res.end("error handling auth code");
            console.error(error);
        }
    }
    else {
        res.writeHead(500);
        res.end(JSON.stringify({ error: "no code returned" }));
        console.error("no code returned", req.url);
        process.exit(1);
    }
};
const requestListener = async (req, res) => {
    res.setHeader("Content-Type", "application/json");
    console.log(req.url);
    if (req.url == "/login") {
        console.log("handleLogin");
        handleLogin(req, res);
    }
    else if (req?.url?.startsWith("/code?")) {
        console.log("handleAuth");
        handleAuth(req, res);
    }
    else {
        res.writeHead(404);
        res.end(JSON.stringify({ error: "not handled method" }));
        console.log("not handled method", req.url);
    }
};
export const main = async ({ port, host, callback }) => {
    createServer(requestListener).listen(port, host, () => {
        console.log(`Server is running on http://${host}:${port}`);
        processTokensFile();
        callback();
    });
};
//# sourceMappingURL=server.js.map