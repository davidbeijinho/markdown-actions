import { writeFileSync, readFileSync, readdirSync } from "fs";
export const writeFile = ({ data, path }) => {
    try {
        writeFileSync(path, data);
        console.log("file written successfully");
    }
    catch (err) {
        console.error(err);
        process.exit(1);
    }
};
export const readFile = ({ path }) => {
    try {
        const data = readFileSync(path, "utf8");
        // console.log({data});
        return data;
    }
    catch (err) {
        console.error(err);
        process.exit(1);
    }
};
export const readFileJson = ({ path }) => {
    try {
        const data = readFile({ path });
        const jsonData = JSON.parse(data);
        // console.log({jsonData});
        return jsonData;
    }
    catch (err) {
        console.error(err);
        process.exit(1);
    }
};
export const readDir = (path) => {
    return readdirSync(path);
};
//# sourceMappingURL=file.js.map