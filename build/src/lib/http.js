import fetch from "node-fetch";
export const postData = async ({ url = "", data = {}, headers = {} }) => {
    //console.log({ url, data, headers });
    // Default options are marked with *
    const response = await fetch(url, {
        method: "POST",
        // mode: "cors", // no-cors, *cors, same-origin
        // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json",
            ...headers,
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        // redirect: "follow", // manual, *follow, error
        // referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
    // console.log({response})
    return response;
};
export const getData = async ({ url = "", headers = {} }) => {
    // console.log({ url, headers });
    // Default options are marked with *
    const response = await fetch(url, {
        method: "GET",
        // mode: "cors", // no-cors, *cors, same-origin
        // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json",
            ...headers,
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        // redirect: "follow", // manual, *follow, error
        // referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
    // console.log({response})
    return response;
};
export const patchData = async ({ url = "", headers = {} }) => {
    // console.log({ url, headers });
    // Default options are marked with *
    const response = await fetch(url, {
        method: "PATCH",
        // mode: "cors", // no-cors, *cors, same-origin
        // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: "same-origin", // include, *same-origin, omit
        headers: {
            "Content-Type": "application/json",
            ...headers,
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        // redirect: "follow", // manual, *follow, error
        // referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        // body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
    // console.log({response})
    return response;
};
export const getJsonData = async ({ url = "", headers = {} }) => {
    const response = await getData({ url, headers });
    return {
        data: await response.json(),
        response
    };
};
//# sourceMappingURL=http.js.map