import { readFile, writeFile } from "./file.js";
import { fromMarkdown } from "mdast-util-from-markdown";
import { frontmatterFromMarkdown, frontmatterToMarkdown, } from "mdast-util-frontmatter";
import { frontmatter } from "micromark-extension-frontmatter";
import { toMarkdown } from "mdast-util-to-markdown";
import { parse } from "yaml";
// let worker: Function;
// let metadata: Object;
const parseDoc = (content) => {
    return fromMarkdown(content, {
        extensions: [frontmatter(["yaml"])],
        mdastExtensions: [frontmatterFromMarkdown(["yaml"])],
    });
};
const parseTree = (data) => {
    if (data.children.length !== 2) {
        throw Error("document has not exactly 2 root children");
    }
    const [yaml, list] = data.children;
    if (yaml.type != "yaml") {
        throw Error("first element is not frontmatter");
    }
    if (list.type != "list") {
        throw Error("second element is not list");
    }
    return { yaml, list };
};
const parseList = async (list, metadata, worker) => {
    // console.log({children: list.children})
    return await {
        ...list,
        children: await Promise.resolve(list.children.reduce((acc, item) => parseItem(acc, item, metadata, worker), Promise.resolve([]))),
    };
};
const parseItem = async (previousItems, item, metadata, worker) => {
    const items = await previousItems;
    //console.log({items, item})
    if (item.children.length != 2) {
        console.error("has not exactly 2 children");
        return [
            ...items,
            {
                ...item,
                ...addError(item, "has not exactly 2 children"),
            },
        ];
    }
    const [paragraph, list] = item.children;
    if (paragraph.type != "paragraph") {
        console.error("first element is not paragraph");
        return [
            ...items,
            {
                ...item,
                ...addError(item, "first element is not paragraph"),
            },
        ];
    }
    if (list.type != "list") {
        console.error("second element is not list");
        return [
            ...items,
            {
                ...item,
                ...addError(item, "second element is not list"),
            },
        ];
    }
    const response = await parseLink(list, metadata, worker);
    if (response === true) {
        return items;
    }
    console.log({ items });
    return [
        ...items,
        {
            ...item,
            children: [paragraph, response],
        },
    ];
};
const parseLink = async (item, metadata, worker) => {
    //console.log(item)
    if (item.children.length != 1) {
        console.error("has not exactly 1 children");
        return addError(item, "has not exactly 1 children");
    }
    const [listItem] = item.children;
    if (listItem.children.length != 1) {
        console.error("has not exactly 1 children");
        return addError(item, "has not exactly 1 children");
    }
    const [paragraph] = listItem.children;
    if (paragraph.type != "paragraph") {
        console.error("first element is not paragraph");
        return addError(item, "first element is not paragraph");
    }
    if (paragraph.children.length != 1) {
        console.error("has not exactly 1 children");
        return addError(item, "has not exactly 1 children");
    }
    const [text] = paragraph.children;
    if (text.type != "text") {
        console.error("first element is not text");
        return addError(item, "first element is not text");
    }
    const response = await worker({ metadata, text: text.value });
    if (response === true) {
        return response;
    }
    if (typeof response === "string") {
        return addError(item, response);
    }
    return addError(item, "not able to save");
};
const addError = (item, error) => {
    return {
        ...item,
        children: [
            ...(item.children || []),
            {
                type: "listItem",
                spread: false,
                children: [
                    {
                        type: "paragraph",
                        children: [{ type: "text", value: error + " #error" }],
                    },
                ],
            },
        ],
    };
};
const saveFile = ({ mdast, yaml, updatedList, path }) => {
    const update = {
        ...mdast,
        children: [yaml, updatedList],
    };
    const data = toMarkdown(update, {
        bullet: "-",
        extensions: [frontmatterToMarkdown(["yaml"])],
    });
    writeFile({
        path,
        data,
    });
    console.log(data);
};
export const processFile = async (path, sender, validator) => {
    const content = await readFile({ path });
    const mdast = parseDoc(content);
    // console.log({ mdast });
    try {
        const { yaml, list } = parseTree(mdast);
        console.log(yaml);
        const metadata = parse(yaml.value);
        await validator({ metadata });
        const updatedList = await parseList(list, metadata, sender);
        saveFile({ mdast, yaml, updatedList, path });
        return updatedList;
    }
    catch (error) {
        console.error(error);
        return error;
    }
};
//# sourceMappingURL=markdown.js.map