import { readDir, writeFile, readFileJson } from "../lib/file.js";
import { processFile } from "../lib/markdown.js";
import { postData, getJsonData } from "../lib/http.js";
import pLimit from "p-limit";

const limit = pLimit(5);
const WALLABAG_URL = process.env.WALLABAG_URL;
const WALLABAG_GRANT_TYPE = process.env.WALLABAG_GRANT_TYPE;
const WALLABAG_CLIENT_ID = process.env.WALLABAG_CLIENT_ID;
const WALLABAG_CLIENT_SECRET = process.env.WALLABAG_CLIENT_SECRET;
const WALLABAG_USERNAME = process.env.WALLABAG_USERNAME;
const WALLABAG_PASSWORD = process.env.WALLABAG_PASSWORD;

const tokenFilePath = "wallabag_token_data.json";

let BEARER: string;

const writeTokenFile = (data: object) =>
  writeFile({ path: tokenFilePath, data: JSON.stringify(data) });

const readTokenFile = () => readFileJson({ path: tokenFilePath });

export const handleAuth = async () => {
  const data = await postData({
    url: WALLABAG_URL + "/oauth/v2/token",
    data: {
      grant_type: WALLABAG_GRANT_TYPE,
      client_id: WALLABAG_CLIENT_ID,
      client_secret: WALLABAG_CLIENT_SECRET,
      username: WALLABAG_USERNAME,
      password: WALLABAG_PASSWORD,
    },
  });
  writeTokenFile(await data.json());
  return data;
};

const processTokensFile = () => {
  const data = readTokenFile();
  BEARER = data.access_token;
  // TODO , check if token is valid if not call getAutToken and set updated BEARER
};

export const sendToWallabag = async ({ metadata, text }) => {
  if (!text.startsWith("http")) {
    return "its not a link";
  }
  if (text.includes("\n")) {
    console.error("text is multiline");
    return "text is multiline";
  }
  // if (text.includes('')) {
  //   console.error("link as a space?");
  //   return "link as a space?"
  // }
  const response = await postData({
    url: WALLABAG_URL + "/api/entries",
    data: {
      url: text,
      tags: metadata.tags,
    },
    headers: {
      Authorization: `Bearer ${BEARER}`,
    },
  });
  // console.log({response})
  const data = await response.json();
  console.log({ data });
  console.log({ status: response.status });
  if (
    data.content.startsWith("wallabag can't retrieve contents for this article")
  ) {
    console.error("fail to get content");
    return "fail to get content";
  }
  return response.status === 200;
};

const validator = async ({ metadata }: { metadata: { tags?: string } }) => {
  console.log({ m: metadata, t: metadata?.tags, tb: !metadata?.tags });
  if (!metadata?.tags) {
    throw Error(
      "no tags found " +
        JSON.stringify({ t: metadata?.tags, x: !metadata?.tags }) +
        " " +
        JSON.stringify(metadata)
    );
  }
};

export const wallabagParser = async ({ dirPath }) => {
  processTokensFile();
  return await Promise.all(
    readDir(dirPath).map(
      async (file) =>
        await processFile(dirPath + file, sendToWallabag, validator)
    )
  );
};

import fs from "fs";

import https from "https";

function getGzipped({ options, folder, item, baseDir, format }, callback) {
  // buffer to store the streamed decompression
  // var buffer = [];
  // const folder = data.tags.find(i=> i.label != "imported").label
  const filename = item.title
    .replace(/[^a-z0-9]/gi, "_")
    .replaceAll("__", "_")
    .replaceAll("__", "_")
    .replaceAll("__", "_")
    .toLowerCase()
    .slice(0, 25);
  // .replaceAll(" ", "_").replaceAll(".", "_").replaceAll("/", "_")
  const file = fs.createWriteStream(`${baseDir}${folder}/${filename}.${format}`);

  https
    .get(options, function (res) {
      res.pipe(file);
      file.on("finish", async () => {
        file.close();
        const response = await postData({
          url: `${WALLABAG_URL}/api/entries/${item.id}/tags`,
          data: {
            tags: "downloaded",
          },
          headers: {
            Authorization: `Bearer ${BEARER}`,
          },
        });
        // const data = await response.json();
        // console.log({data})
        // console.log({response})
        if (response.status !== 200) {
          callback("fail to upate with tag");
        }
        console.log("Download Completed");
        callback(null, "Download Completed");
      });
    })
    .on("error", function (e) {
      console.log("error");
      callback(e);
    });
}

const handleItem = async ({item, dirPath, format}) => {
  if (item.tags.find((i) => i.label === "downloaded")) {
    return "skip already downloaded";
  }

  const options = {
    hostname: "wallabag.hive.thebeijinho.com",
    path: `/api/entries/${item.id}/export.${format}`, 
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${BEARER}`,
    },
  };
  const baseDir = dirPath;
  const folder = item.tags.find((i) => i.label != "imported" && i.label != "downloaded").label || "default";
  console.log({ baseDir, folder });
  if (!fs.existsSync(baseDir + folder)) {
    fs.mkdirSync(baseDir + folder);
  }

  try {
    return await limit(
      () =>
        new Promise((resolve, reject) => {
          getGzipped(
            {
              options,
              folder,
              baseDir,
              item,
              format
            },
            function (err, data) {
              if (err) {
                console.error(err);
                reject(data);
              }
              console.log(data);
              resolve(data);
            }
          );
        })
    );
  } catch (error) {
    return error;
  }
};

const downloadEntries = async ({tag, dirPath, page, format}: {tag: string, dirPath: string, page: number, format: string}) => {
  if (tag === "imported") {
    // console.log("will not handle imported tag");
    return "will not handle imported tag";
  }
  if (tag === "downloaded") {
    // console.log("will not handle downloaded tag");
    return "will not handle downloaded tag";
  }
  const { data } = await getJsonData({
    url:
      WALLABAG_URL +
      `/api/entries?detail=metadata&archive=0&tags=${tag}&page=${page}`,
    headers: {
      Authorization: `Bearer ${BEARER}`,
    },
  });
  //console.log({ data, response, items: data._embedded.items });
  // const items = [data._embedded.items[0]]
  const items = data._embedded.items;
  const itemsResponse = await Promise.all(
    items.map(async (i) => await handleItem({item: i, dirPath, format}))
  );
  // console.log(itemsResponse);
  if (data.pages > page) {
    return await downloadEntries({tag, dirPath, page: page + 1, format});
  }
  return itemsResponse;
};

export const downloadFiles = async ({ dirPath }) => {
  processTokensFile();
  const format = "epub"
  //console.log({ dirPath });
  const { data } = await getJsonData({
    url: WALLABAG_URL + "/api/tags",
    headers: {
      Authorization: `Bearer ${BEARER}`,
    },
  });
  // console.log({ data });
  const mockEntries = data;
  // const mockEntries = [data[1]]
  const response = await mockEntries.map(async (tagData) => {
    return await downloadEntries({tag: tagData.label, dirPath, page: 1, format});
  });
  return await Promise.all(response);
};
