import { writeFileSync, readFileSync, readdirSync } from "fs";

export const writeFile = ({ data, path}: {data: string, path:string}) => {
  try {
    writeFileSync(path, data);
    console.log("file written successfully");
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

export const readFile =  ({ path }: {path: string}) => {
  try {
    const data = readFileSync(path, "utf8");
    // console.log({data});
    return data
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

export const readFileJson = ({ path }:{path: string}) => {
  try {
    const data = readFile({path});
    const jsonData = JSON.parse(data);
    // console.log({jsonData});
    return jsonData
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

export const readDir = (path:string) => {
  return readdirSync(path)
}
