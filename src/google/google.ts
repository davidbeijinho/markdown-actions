import type { Auth } from "googleapis";
import { google } from "googleapis";
import { writeFile, readFileJson } from "../lib/file.js";

const clientId = process.env.GOOGLE_API_CLIENT_ID
const clientSecret = process.env.GOOGLE_API_CLIENT_SECRET
const redirectUri = process.env.GOOGLE_API_REDIRECT_URI;

const oauth2Client = new google.auth.OAuth2({
  clientId,
  clientSecret,
  redirectUri,
});

const scopes = ["https://www.googleapis.com/auth/youtube"];

// const tokenFilePath = "../../data/tokendata.json";
const tokenFilePath = "google_token_data.json";

const writeTokenFile = (data: object) => writeFile({path: tokenFilePath, data: JSON.stringify(data)})

const readTokenFile = () => readFileJson({path: tokenFilePath});

export const generateAuthUrl = () => {
  const url = oauth2Client.generateAuthUrl({
    // 'online' (default) or 'offline' (gets refresh_token)
    access_type: "offline",
    scope: scopes,
  });

  return url;
};

export const handleAuthCode = async (code:string) => {
  const { tokens } = await oauth2Client.getToken(code);

  writeTokenFile(tokens);

  setCredentials(tokens);
};

const setCredentials = (tokens: Auth.Credentials) => {
  try {
    oauth2Client.setCredentials(tokens);
    google.options({
      auth: oauth2Client,
    });
  } catch (error) {
    console.error(error);
  }
};

export const processTokensFile = () => {
  const tokens = readTokenFile();
  setCredentials(tokens);
};
