import { google } from "googleapis";
import { processTokensFile  } from "./google.js";
import { readDir } from "../lib/file.js";
import { processFile } from "../lib/markdown.js";
export const youtube = google.youtube("v3");

export async function playlistItemsInsert({ playlistId, videoId }:{playlistId: string, videoId: string}) {
  try {
    console.log({ playlistId, videoId });
    const response = await youtube.playlistItems.insert({
      requestBody: {
        snippet: {
          playlistId,
          resourceId: {
            kind: "youtube#video",
            videoId,
          },
        },
      },
      part: ["snippet"],
    });
    console.log({response})
    console.log({data: response.data.snippet})
    if (response.status === 403) {
      console.error({response})
      console.error("limit of quota")
      process.exit(1)
    }
    return response.status === 200;
  } catch (error) {
    console.error(error)
    console.error("error sending video" + JSON.stringify(error));
    return JSON.stringify(error);
  }
}

export const saveToPlaylist = async ({
  metadata,
  text,
}: {
  metadata: { playlistId?: string };
  text: string;
}): Promise<String | Boolean> => {
  console.log({metadata,text})
  if (!metadata.playlistId) {
    console.error("no playlist id found");
    return "no playlist id found"
  }
  if (text.includes('\n')) {
    console.error("text is multiline");
    return "text is multiline"
  }

  try {
    console.log({text, par: JSON.stringify(text)})
    const url = new URL(text);
    const pathHostnames = ["youtu.be", "www.youtu.be"];
    const paramsHostnames = ["youtube.com", "www.youtube.com", 'm.youtube.com'];
    const validHostnames = [...paramsHostnames, ...pathHostnames];
    if (!validHostnames.includes(url.hostname)) {
      return "hostname not valid";
    }
    let videoId;
    if (pathHostnames.includes(url.hostname)) {
      videoId = getIdFromPath(url);
    } else {
      videoId = getIdFromParams(url);
    }
    if (!videoId?.length) {
      return "video id not valid (length) " + videoId + ' ' + videoId?.length;
    } else {
      const response = await playlistItemsInsert({
        playlistId: metadata.playlistId,
        videoId,
      });
      console.log({ response });
      if (response === true) {
        return true;
      }
      return "error sending video " + response;
    }
  } catch (error) {
    console.error("url is not valid", error);
    return "url is not valid";
  }
};

const getIdFromPath = (url: URL) => {
  // console.log(url.pathname)
  return url.pathname.slice(1);
};

const getIdFromParams = (url: URL) => {
  return url.searchParams.get("v");
};

const validator = async ({metadata}: {metadata: {playlistId?: string}}) => {
  if (!metadata?.playlistId) {
    throw Error("no playlist id found")  
  }
}

export const youtubeParser = async ({dirPath}) => {
  processTokensFile();
  return await Promise.all(
    readDir(dirPath).map(
      async (file) => await processFile(dirPath + file, saveToPlaylist, validator)
    )
  );
};