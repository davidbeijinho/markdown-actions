import "dotenv/config";
import { youtubeParser } from "./google/youtube.js";
import { handleAuth, wallabagParser, downloadFiles } from "./wallabag/main.js";
import { generateAuthUrl, handleAuthCode } from "./google/google.js";
import { program, Argument } from "commander";

program
  .name("markdown-actions")
  .description("CLI to some markdown utilities")
  .version("0.0.1");

program
  .command("youtube")
  .description("handle youtube actions")
  .addArgument(
    new Argument("<action>", "action name").choices(["auth", "code", "send"])
  )
  .addArgument(new Argument("[url]", "url callback from google"))
  .action(async (flags, options) => {
    console.log({ flags, options });
    if (flags == "auth") {
      if (options) {
        console.error("no options expected");
        process.exit(1);
      }
      console.log("handle auth youtube");
      const url = await generateAuthUrl();
      console.log(url);
      process.exit(0);
    } else if (flags == "code") {
      if (!options) {
        console.error("url is mandatory");
        process.exit(1);
      }
      console.log("handle code");
      const response = await handleAuthCode(options);
      console.log({ response });
      process.exit(0);
    }
    if (!options) {
      console.error("path is mandatory");
      process.exit(1);
    }
    console.log("handle send youtube");
    const youtube = await youtubeParser({dirPath: options});
    console.log({ youtube });
    process.exit(0);
  });

program
  .command("wallabag")
  .description("handle wallabag actions")
  .addArgument(
    new Argument("<action>", "action name").choices(["auth", "send", "download"])
  )
  .addArgument(new Argument("[path]", "directory path"))
  .action(async (flags, options) => {
    console.log({ flags, options });
    if (flags == "auth") {
      if (options) {
        console.error("no options expected");
        process.exit(1);
      }
      console.log("handle auth youtube")
      const data = await handleAuth();
      console.log(data);
      process.exit(0);
    }
    if (flags == "download") {
      if (!options) {
        console.error("path is mandatory");
        process.exit(1);
      }
      console.log("handle download")
      const data = await downloadFiles({dirPath: options});
      console.log(data);
      process.exit(0);
    }
    if (!options) {
      console.error("path is mandatory");
      process.exit(1);
    }
    console.log("handle send wallabag");
    const response = await wallabagParser({dirPath: options});
    console.log({ response });
    process.exit(0);
  });

program.parse();
